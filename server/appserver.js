var express = require("express");
var bodyParser = require('body-parser');
var Sequelize = require ("sequelize");

var node_port = process.env.PORT || 3000;
const GROCERY_API = "/api/grocery";

//console.log(Sequelize);
const SQL_USERNAME="root";
const SQL_PASSWORD="1111"
var connection = new Sequelize(
    'grocerybd',
    SQL_USERNAME,
    SQL_PASSWORD,
    {
        host: 'localhost',
        port: 3308,
        logging: console.log,
        dialect: 'mysql',
        pool: {
            max: 10,
            min: 0,
            idle: 20000,
            acquire: 20000
        }
    }
);

var Grocerylist = require('./models/grocery')(connection, Sequelize);

var app = express();
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({limit: '50mb'}));

app.use(express.static(__dirname + "/../client/"));


// retrieve 
app.get(GROCERY_API, (req, res)=>{
    console.log("search > " + req.query.keyword);
    var keyword = req.query.keyword;
    var sortby = req.query.sortby;
    var itemsPerPage = parseInt(req.query.itemsPerPage);
    var currentPage = parseInt(req.query.currentPage);
    var offset = (currentPage - 1) * itemsPerPage;
    if(sortby == 'null'){
        console.log("sortby is null");
        sortby = "ASC";
    }

    console.log(keyword);
    console.log(itemsPerPage);
    console.log(currentPage);
    console.log(typeof offset);
    
    console.log("sortby " + sortby);
    console.log(typeof(keyword));
    if(keyword == ''){
        console.log("keyword is empty ?");
    }
    var whereClause = { offset: offset, limit: itemsPerPage, order: [['name', sortby], ['name', sortby]]};
    console.log(keyword.length);
    console.log(keyword !== 'undefined');
    console.log(keyword != undefined);
    console.log(!keyword);
    console.log(keyword.trim().length > 0);

    if((keyword !== 'undefined' || !keyword) && keyword.trim().length > 0){
        console.log("> " + keyword);
        whereClause = { offset: offset, limit: itemsPerPage, order: [['brand', sortby], ['brand', sortby]], where: {$or: [{brand: {like: '%'+keyword+'%'}, name: {like: '%'+keyword+'%'} }]} };
    }
    console.log(whereClause);
    Grocerylist.findAndCountAll(whereClause).then((results)=>{
        res.status(200).json(results);
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    });    
});

app.get(GROCERY_API+"/:id", (req, res)=>{
    console.log("one grocery ...");
    console.log(req.params.id);
    var id = req.params.id;
    console.log(id);
    var whereClause = {limit: 1, where: {id: id}};
    Grocerylist.findOne(whereClause).then((result)=>{
        res.status(200).json(result);
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    });    
});

// create
app.post(GROCERY_API, (req, res)=>{
    console.log(">>> " + JSON.stringify(req.body));
    var grocery = req.body;
    Grocerylist.create(grocery).then((result)=>{
        console.log(result);
        res.status(200).json(result);
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    });
});

// update
app.put(GROCERY_API, (req, res)=>{
    console.log(">>> " + JSON.stringify(req.body));
    console.log("update grocery ..." + req.body);
    console.log(req.body.id);
    var id = req.body.id;
    console.log(id);
    var whereClause = {limit: 1, where: {id: id}};
    Grocerylist.findOne(whereClause).then((result)=>{
        result.update(req.body);
        res.status(200).json(result);
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    });    
});









app.use(function (req, res) {
    res.send("<h1>Page not found</h1>");
});

app.listen(node_port, function () {
    console.log("Server running at http://localhost:" + node_port);
});

module.exports = app;


/*// create




app.delete(EMPLOYEE_API+"/:emp_no", (req, res)=>{
    console.log("one employee ...");
    console.log(req.params.emp_no);
    var emp_no = req.params.emp_no;
    console.log(emp_no);
    var whereClause = {limit: 1, where: {emp_no: emp_no}};
    Employees.findOne(whereClause).then((result)=>{
        result.destroy();
        res.status(200).json({});
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    }); 
});

*/