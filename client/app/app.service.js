(function() {
    angular
    .module("GMMApp")
    .service("GMMAppAPI", [
        '$http', 
        '$state',
        GMMAppAPI
    ]);

    function GMMAppAPI($http,$state) { 
        var self = this;

        self.searchGrocerylist = function(keyword_value, sortby, itemsPerPage, currentPage) {

            return $http.get(`/api/grocery?keyword=${keyword_value}&sortby=${sortby}&itemsPerPage=${itemsPerPage}&currentPage=${currentPage}`);
            //return $http.get("/api/employees?keyword=" + keyword_value); //(3) because of $http it calls the get
        }                           //MULTIPLE ?keyword1=" + value1 + "&keyword2=" + value2

        self.getGrocery = function(id) { //create an end point in app.js
            console.log(id);
            return $http.get("/api/grocery/" + id) //this is parameter (check app.js)
        }

        self.updateGrocery = function(grocery) { 
            console.log(grocery);
            return $http.put("/api/grocery", grocery);
        }

       self.addGrocery = function(grocery) {
            return $http.post("/api/grocery", grocery);
        }

//        self.deleteGrocery = function(emp_no) {
//            console.log(id);                             //PARAMETERIZE means xx/xx/ 
//            return $http.delete("/api/grocery/" + emp_no); //parameterize it on the URL 
//        }
   }

})();