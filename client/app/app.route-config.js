(function () {  

angular
.module("GMMApp")
.config(uirouterAppConfig);
uirouterAppConfig.$inject = ["$stateProvider","$urlRouterProvider"];


function uirouterAppConfig($stateProvider, $urlRouterProvider){

$stateProvider
    .state("home",{ //localhost:3000/#!/A 
        url : '/home',
        templateUrl: "home.html",
        controller : 'GroceryController', //Ok to use different controller (1 page 1 controller)
        controllerAs : 'ctrl'
    })
    .state("add", {
        url: "/add",
        templateUrl: "addgrocery.html",
        controller : 'AddGroceryCtrl',
        controllerAs : 'ctrl'
    })

    .state("edit", {
        url: "/edit",
        templateUrl: "editgrocery.html",
        controller : 'EditGroceryCtrl',
        controllerAs : 'ctrl'
    })

    $urlRouterProvider.otherwise("/home"); //if can't find any route, it goes to A 

}

})();