(function () {
    angular
        .module("GMMApp")
        .controller("GroceryController", GroceryController)
        .controller("EditGroceryCtrl", EditGroceryCtrl)
        .controller("AddGroceryCtrl", AddGroceryCtrl);
    //  .controller("DeleteEmployeeCtrl", DeleteEmployeeCtrl);
        
    GroceryController.$inject = ['GMMAppAPI', '$uibModal', '$document','$scope'];
    EditGroceryCtrl.$inject = ['$uibModalInstance', 'GMMAppAPI','items', '$rootScope','$scope'];
    AddGroceryCtrl.$inject = ['GMMAppAPI', '$rootScope', '$scope','$state'];
   // DeleteGrocery.$inject = ['$uibModalInstance', 'EMSAppAPI', 'items', '$rootScope', '$scope'];

   //function AddGroceryCtrl($uibModalInstance, GMMAppAPI, items, $rootScope, $scope){
   function AddGroceryCtrl(GMMAppAPI, $rootScope, $scope, $state){
    console.log("Add Grocery");
    var self = this;
    self.saveGrocery = saveGrocery;
    self.cancelAddGrocery = cancelAddGrocery;
    self.afterAddGrocery = afterAddGrocery;

    function afterAddGrocery(id){
        
         $state.go('home');
       
    }
   
    function saveGrocery(){
        console.log("save grocery ...");
        console.log(self.grocery.id);
        console.log(self.grocery.upc12);
        console.log(self.grocery.brand);
        console.log(self.grocery.name);

        GMMAppAPI.addGrocery(self.grocery).then((result)=>{
            //console.log(result);
            console.log("Add grocery -> " + result.id);
            $rootScope.$broadcast('refreshGroceryListFromAdd', result.data);
           afterAddGrocery(self.grocery.id);
         }).catch((error)=>{
            console.log(error);
            self.errorMessage = error;
         })



    }




    function cancelAddGrocery(){
        
            $state.go('home');

            }

}



   function EditGroceryCtrl($uibModalInstance, GMMAppAPI, items, $rootScope){
    console.log("Edit grocery Ctrl");
    var self = this;
    console.log("items > " + items);
    console.log("self.items1 > " + self.items);
    self.items = items;
    console.log("self.items2 > " + self.items);

    GMMAppAPI.getGrocery(items).then((result)=>{
       console.log(result.data);
       self.grocery =  result.data;

    })

    self.saveGrocery = saveGrocery;
    self.cancelEditGrocery = cancelEditGrocery;

    function saveGrocery(){
        console.log("save grocery ...");
        console.log(self.grocery.id);
        console.log(self.grocery.last_name);
        console.log(self.grocery.brand);
        console.log(self.grocery.name);

        GMMAppAPI.updateGrocery(self.grocery).then((result)=>{
            console.log(result);
            $rootScope.$broadcast('refreshGroceryList');
         }).catch((error)=>{
            console.log(error);
         })
        $uibModalInstance.close(self.run);
    }

    function cancelEditGrocery(){

        $uibModalInstance.close(self.run);
    }


}



    function GroceryController(GMMAppAPI, $uibModal, $document,$scope) {
        var self = this;
        
        self.format = "EEEE, MMMM d, y h:mm:ss a";

        self.grocerylist = [];
        self.maxsize=5;
        self.totalItems = 0;
        self.itemsPerPage = 20;
        self.currentPage = 1;

        self.searchGrocerylist =  searchGrocerylist;
       // self.addgrocerylist =  addgrocerylist;
        self.editGrocery = editGrocery;
      //  self.deletegrocerylist = deletegrocerylist;
        self.pageChanged = pageChanged;
        self.startload = startload;

        function searchGrocerylist(){
            console.log("search grocerylist Start  ....");
            console.log(self.orderby);
            searchAllGrocerylist(self.searchKeyword, self.orderby,self.itemsPerPage, self.currentPage);
        }


        function searchAllGrocerylist(searchKeyword,orderby,itemsPerPage,currentPage){
            GMMAppAPI.searchGrocerylist(searchKeyword, orderby, itemsPerPage, currentPage).then((results)=>{
                console.log("grocerylist 1 > " + self.grocerylist);
                console.log("totalItems1  > " + self.totalItems);
                self.grocerylist = results.data.rows;
                console.log("grocerylist 2 > " + self.grocerylist);
                self.totalItems = results.data.count;
                console.log("totalItems2  > " + self.totalItems);

                $scope.numPages = Math.ceil(self.totalItems /self.itemsPerPage);
            }).catch((error)=>{
                console.log(error);
            });
        }

        function pageChanged(){
            console.log("Page changed " + self.currentPage);
            searchAllGrocerylist(self.searchKeyword, self.orderby,self.itemsPerPage, self.currentPage);
            console.log($scope.numPages);
        }

        $scope.$on("refreshGroceryList",function(){
            console.log("refresh grocery list "+ self.searchKeyword);
            searchAllGrocerylist(self.searchKeyword, self.orderby,self.itemsPerPage, self.currentPage);
        });

        $scope.$on("refresgroceryFromAdd",function(event, args){
            console.log("refresh grocery list from id"+ args.id);
            var grocery = [];
            grocery.push(args);
            self.searchKeyword = "";
            self.grocerylist = grocery;

        });

        function editGrocery(id, size, parentSelector){
            console.log("Edit grocery...");
            console.log("id > " + id);
            
            var parentElem = parentSelector ? 
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './editgrocery.html',
                controller: 'EditGroceryCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return id;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
        }

        function startload(){

            var startsearchKeyword = 'undefined';
            var startorderby = "ASC"
            console.log("startload ....");
            console.log(startsearchKeyword);
            console.log(startorderby);
            console.log(self.itemsPerPage);
            console.log(self.currentPage);
          
            searchAllGrocerylist(startsearchKeyword, startorderby,self.itemsPerPage, self.currentPage);

        }

        var i = 0;
        console.log("i1 > " + i);
        if (i == 0){
            startload();
            i++;
            console.log("i2 > " + i);
        }
      
 
    }



    
})();


/*


 function addGrocery(size, parentSelector){
            console.log("post add grocery  ....");
            var items = [];
            var parentElem = parentSelector ? 
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './app/addgrocery.html',
                controller: 'AddGroceryCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return items;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
            
        }




 



    function DeleteEmployeeCtrl($uibModalInstance, EMSAppAPI, items, $rootScope, $scope){
        var self = this;
        //self.items = items;
        self.deleteEmployee = deleteEmployee;
        console.log(items);
        EMSAppAPI.getEmployee(items).then((result)=>{
            console.log(result.data);
            self.employee =  result.data;
            self.employee.birth_date = new Date( self.employee.birth_date);
            self.employee.hire_date = new Date( self.employee.hire_date);
            console.log(self.employee.birth_date);
        });

        function deleteEmployee(){
            console.log("delete employee ...");
            EMSAppAPI.deleteEmployee(self.employee.emp_no).then((result)=>{
                console.log(result);
                $rootScope.$broadcast('refreshEmployeeList');
                $uibModalInstance.close(self.run);
            }).catch((error)=>{
                console.log(error);
            });
        }

    }




   




    function EditEmployeeCtrl($uibModalInstance, EMSAppAPI, items, $rootScope, $scope){
        console.log("Edit Employee Ctrl");
        var self = this;
        self.items = items;
        initializeCalendar($scope);

        EMSAppAPI.getEmployee(items).then((result)=>{
           console.log(result.data);
           self.employee =  result.data;
           self.employee.birth_date = new Date( self.employee.birth_date);
           self.employee.hire_date = new Date( self.employee.hire_date);
           console.log(self.employee.birth_date);
        })

        self.saveEmployee = saveEmployee;

        function saveEmployee(){
            console.log("save employee ...");
            console.log(self.employee.first_name);
            console.log(self.employee.last_name);
            console.log(self.employee.hire_date);
            console.log(self.employee.birth_date);
            console.log(self.employee.gender);
            EMSAppAPI.updateEmployee(self.employee).then((result)=>{
                console.log(result);
                $rootScope.$broadcast('refreshEmployeeList');
             }).catch((error)=>{
                console.log(error);
             })
            $uibModalInstance.close(self.run);
        }

    }






 



 function deleteEmployee(emp_no, size, parentSelector){
            console.log("delete Employee...");
            console.log("emp_no > " + emp_no);
            
            var parentElem = parentSelector ? 
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './app/deleteEmployee.html',
                controller: 'DeleteEmployeeCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return emp_no;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
        }


*/